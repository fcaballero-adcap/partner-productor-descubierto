package banza.partner.productor.descubierto;

import java.text.DecimalFormat;
import java.util.List;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.rdsdata.AWSRDSData;
import com.amazonaws.services.rdsdata.AWSRDSDataClientBuilder;
import com.amazonaws.services.rdsdata.model.ExecuteStatementRequest;
import com.amazonaws.services.rdsdata.model.ExecuteStatementResult;
import com.amazonaws.services.rdsdata.model.Field;

import banza.partner.productor.descubierto.dto.DescubiertoDto;
import banza.partner.productor.descubierto.dto.Params;

public class HandlerProductorDescubierto implements RequestHandler<Params, DescubiertoDto> {

	@Override
	public DescubiertoDto handleRequest(Params event, Context context) {
		LambdaLogger logger = context.getLogger();
		AWSRDSData rdsData = AWSRDSDataClientBuilder.standard()
				.withEndpointConfiguration(
						new AwsClientBuilder.EndpointConfiguration(System.getenv("ENDPOINT"), System.getenv("REGION")))
				.build();
//		logger.log(
//				"" + String.format(System.getenv("QUERY_TENENCIA_CONSOLIDADA"), event.getIdProductor(), MONEDA_PESO));
		ExecuteStatementRequest request = new ExecuteStatementRequest().withResourceArn(System.getenv("RESOURCE_ARN"))
				.withSecretArn(System.getenv("SECRET_ARN")).withDatabase(System.getenv("BD"))
				.withSql(String.format(System.getenv("QUERY_DESCUBIERTO_PESOS"), event.getIdProductor()));
		ExecuteStatementResult result = rdsData.executeStatement(request);
		logger.log("" + result);
		String descubiertoPesos = "0.00";
		if (!result.getRecords().isEmpty()) {
			for (List<Field> fields : result.getRecords()) {
				if (fields.get(0).getDoubleValue() != null && fields.get(0).getDoubleValue() < 0) {
					descubiertoPesos = new DecimalFormat("0.00").format(Math.abs((fields.get(0).getDoubleValue())));
				}
			}
		}

		request.withSql(String.format(System.getenv("QUERY_DESCUBIERTO_DOLARES"), event.getIdProductor()));
		result = rdsData.executeStatement(request);
		logger.log("" + result);
		String descubiertoDolares = "0.00";
		if (!result.getRecords().isEmpty()) {
			for (List<Field> fields : result.getRecords()) {
				if (fields.get(0).getDoubleValue() != null && fields.get(0).getDoubleValue() < 0) {
					descubiertoDolares = new DecimalFormat("0.00").format(Math.abs((fields.get(0).getDoubleValue())));
				}
			}
		}
		DescubiertoDto descubierto = new DescubiertoDto();
		descubierto.setDescubiertoPesos(descubiertoPesos);
		descubierto.setDescubietoDolares(descubiertoDolares);
		return descubierto;
	}
}
package banza.partner.productor.descubierto.dto;

public class DescubiertoDto {

	private String descubiertoPesos;

	private String descubietoDolares;

	public String getDescubiertoPesos() {
		return descubiertoPesos;
	}

	public String getDescubietoDolares() {
		return descubietoDolares;
	}

	public void setDescubiertoPesos(String descubiertoPesos) {
		this.descubiertoPesos = descubiertoPesos;
	}

	public void setDescubietoDolares(String descubietoDolares) {
		this.descubietoDolares = descubietoDolares;
	}

}


# Requerimientos


- [Java 8 runtime environment (SE JRE)](https://www.oracle.com/java/technologies/javase-downloads.html)
- [Maven 3](https://maven.apache.org/docs/history.html)


# Compilación


Sobre el directorio del proyecto ejecutar el siguiente comando maven

    $ mvn install


# Configuración de la función Lambda


Para configurar la función lambda se deben definir las siguientes variables de entorno:

Clave "BD" - Valor "nombre de la base"

Clave "ENDPOINT" - Valor "https://rds-data.us-east-1.amazonaws.com"

Clave "QUERY_DESCUBIERTO_DOLARES" - Valor "select sum(Cantidad) from tenenciaval where productor_id=%s and tpActivo='Dolares'"

Clave "QUERY_DESCUBIERTO_PESOS" - Valor "select sum(TenenciaVal) from tenenciaval where productor_id=%s and tpActivo='Pesos'"

Clave "REGION" - Valor "us-east-1"

Clave "RESOURCE_ARN" - Valor "resource arn de aurora"

Clave "SECRET_ARN" - Valor "secret arn de aurora"


# Despliegue

Cargar en la función lambda el .jar generado en el directorio ..\partner-productor-descubierto\target


# Test

Ejemplo JSON de entrada:

    {
      "idProductor": 83
    }
    
Ejemplos JSON respuesta:

    {
      	"descubiertoPesos": "8977107.96",
      	"descubietoDolares": "12921.85"
    }
  
